# QGIS export in CSV
Scritp python per QGIS che genera un file CSV dove vengono esportati i valori delle features selezionate.
Vengono esportati tutti i valori in automatico.
Lo script è stato elaborato per essere utilizzato con un qualsiasi layer ed è possibile utilizzarlo come azione applicata al layer.
Per esportare i dati è necessario selezionare prima una o più features.
In caso di utilizzo come azione cliccare su qualsiasi feature, verranno esportati solamenti i dati delle features precedentemente selezionate.
Lo script è stato creato per utilizzare il file CSV generato in una stampa unione di un modello di LibreOffice e creare, in tal modo, dei documenti standard come possono esserlo, ad esempio, le autorizzazioni.
