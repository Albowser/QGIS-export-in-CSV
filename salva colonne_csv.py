from qgis.core import *
from qgis.utils import *
import csv
layer=iface.activeLayer()
campi=layer.fields()#elenco dei campi
nrCol=len(campi)#quantità di campi
contaCol=(range(0,nrCol,1))
intestazioni=[]#inzizializza l'elenco intestazioni
rigaVal=[]#inzizializza l'elenco rigaVal
listaVal=[]#inzizializza l'elenco listaVal
for colNr in contaCol:
    nome=campi.at(colNr).name()
    intestazioni.append(nome)
with open('prova.csv',mode='w') as csv_file:#imposta il file per la scrittura
    writer = csv.writer(csv_file, dialect='excel', quoting=csv.QUOTE_ALL)#imposta l'esportazione con il modulo csv
    writer.writerow(intestazioni)#scrive la prima riga con le intestazioni
    for f in layer.getSelectedFeatures():
        rigaVal=[]
        for colNr in contaCol:
            rigaVal.append(f[colNr])
        listaVal.append(rigaVal)
    writer.writerows(listaVal)
